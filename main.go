package main

import (
	"fmt"

	"github.com/davecgh/go-spew/spew"
)

var (
	// set by goreleaser
	version string
	commit  string
	date    string
	builtBy string
)

func main() {
	spew.Dump("change")
	fmt.Printf("Version: %s\nCommit: %s\nDate: %s\nBuilt by: %s\n", version, commit, date, builtBy)
}
